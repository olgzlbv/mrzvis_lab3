#include "pch.h"
#include "Matrix.h"



Matrix::Matrix(size_t x, size_t y)
    : data(x, Row(y, 0))
{
}

Matrix::Matrix(const Row& row)
    : data(1, row)
{
}

Matrix::Row& Matrix::operator[](size_t n)
{
    return data[n];
}

const Matrix::Row& Matrix::operator[](size_t n) const
{
    return data[n];
}

size_t Matrix::getX() const
{
    return data.size();
}

size_t Matrix::getY() const
{
    if (getX() == 0)
        return 0;
    else
        return data[0].size();
}

Matrix Matrix::transpose()
{
    const size_t x = getX();
    const size_t y = getY();

    Matrix transposed(y, x);

    for (size_t j = 0; j < y; ++j)
        for (size_t i = 0; i < x; ++i)
            transposed[j][i] = (*this)[i][j];

    return std::move(transposed);
}

Matrix Matrix::operator*(const Matrix& second) const
{
    const size_t x1 = getX();
    const size_t y1 = getY();
    const size_t y2 = second.getY();

    Matrix result(x1, y2);

    for (size_t i = 0; i < x1; ++i)
        for (size_t j = 0; j < y2; ++j)
            for (size_t k = 0; k < y1; ++k)
                result[i][j] += (*this)[i][k] * second[k][j];

    return std::move(result);
}

Matrix Matrix::operator*(int n) const
{
    const size_t x = getX();
    const size_t y = getY();

    Matrix result(x, y);

    for (size_t i = 0; i < x; ++i)
        for (size_t j = 0; j < y; ++j)
            result[i][j] = (*this)[i][j] * n;

    return std::move(result);
}

Matrix Matrix::operator-(const Matrix& second) const
{
    const size_t x = getX();
    const size_t y = getY();

    Matrix result(x, y);

    for (size_t i = 0; i < x; ++i)
        for (size_t j = 0; j < y; ++j)
            result[i][j] = (*this)[i][j] - second[i][j];

    return std::move(result);
}

Matrix operator* (int n, const Matrix& matrix)
{
    return matrix * n;
}
