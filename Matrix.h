#pragma once

#include <vector>

using std::size_t;

//author - Yana Lavrinovich
class Matrix
{
public:
    using Row = std::vector<int>;

    Matrix(size_t x = 0, size_t y = 0);
    Matrix(const Row& row);

    inline size_t getX() const;
    inline size_t getY() const;

    Row& operator[] (size_t n);
    const Row& operator[] (size_t n) const;

    Matrix transpose();

    Matrix operator* (const Matrix& second) const;
    Matrix operator* (int n) const;

    Matrix operator- (const Matrix& second) const;

    friend Matrix operator* (int n, const Matrix& matrix);

private:
    std::vector<Row> data;
};

Matrix operator* (int n, const Matrix& matrix);
