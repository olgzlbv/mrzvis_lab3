

#ifndef BIASSOCIATIVEMEMORY_H
#define BIASSOCIATIVEMEMORY_H

#include <fstream>
#include <iostream>
#include <vector>
#include "Matrix.h"

using namespace std;

#define DOT '.'
#define GRID '#'

class BiAssociativeMemory
{
public:
	BiAssociativeMemory();
	void Recognize();

private:
	int n;  //������ ������
	int p;  //������ ���������� � ���������� ����� � ������� ������
	int m;  //���������� ����� � ������� ������ m*p=n
	int Q;  //���������� ��� ����� - ����������, �� ������� ������� ����
	double L;  //������� ����
	Matrix W;  //���� ������� ���� p*n
	Matrix W_; //���� ������� ���� n*p

	Matrix Xk; //������ �������
	Matrix Yk; //������ ����������

	string imgX;    //������ �������� ������
	string imgY;    //������ ������� ����������

	vector<int> vecX;    //������ �������� ������
	vector<int> vecY;    //������ ������� ����������

	string imgX_;   //������ ����������� ������
	vector<int> vecX_; //���������� ���������� �����

	vector<int> cur_vecX_; //�������� ����������� ������ �� ������� ��������
	vector<int> cur_vecY_; //�������� ����������� ������ �� ������� ��������
	vector<int> old_vecX_; //�������� ����������� ������ �� ���������� ��������
	vector<int> old_vecY_; //�������� ����������� ������ �� ���������� ��������

	void ReadFromFileImg_Ass(string path);   //������� ���� ����� - ����������
	void ReadFromFileRecognImage(string path);   //������� ����� ��� �������������
	double CountL();  //��c������ L
	Matrix VecToMat(vector<int> v);
	vector<int> StringToVec(string str);
	string VecToString(vector<int> X);
	void PrintImg(string img);
	void CountWeigths();
	void AddX_Y(Matrix X, vector<int> Y);  //�������� ���� ����� - ����������
	int F(int s);
};

#endif // BIASSOCIATIVEMEMORY_H
